package ru.kolesnikov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.entity.Project;
import ru.kolesnikov.tm.entity.Task;
import ru.kolesnikov.tm.entity.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public final class Domain implements Serializable {

    @Nullable
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    private List<Project> projects = new ArrayList<>();

    @Nullable
    private List<User> users = new ArrayList<>();

}