package ru.kolesnikov.tm.service;

import ru.kolesnikov.tm.api.repository.ICommandRepository;
import ru.kolesnikov.tm.api.service.ICommandService;
import ru.kolesnikov.tm.command.AbstractCommand;

import java.util.List;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

}