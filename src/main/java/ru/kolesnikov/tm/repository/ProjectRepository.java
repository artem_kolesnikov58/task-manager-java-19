package ru.kolesnikov.tm.repository;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.api.repository.IProjectRepository;
import ru.kolesnikov.tm.entity.Project;
import ru.kolesnikov.tm.exception.system.UnknownIdException;
import ru.kolesnikov.tm.exception.system.UnknownNameCommand;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository implements IProjectRepository {

    @NotNull
    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (!userId.equals(project.getUserId())) return;
        projects.remove(project);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Project> projects = findAll(userId);
        this.projects.removeAll(projects);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Project project: projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (id.equals(project.getId())) return project;
        }
        throw new UnknownIdException();
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<Project> projects = findAll(userId);
        return projects.get(index);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final Project project: projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        throw new UnknownNameCommand();
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project removeOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Project project = findOneById(userId, id);
        projects.remove(project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Project project = findOneByIndex(userId, index);
        remove(userId, project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project removeOneByName(@NotNull final String userId, @NotNull final String name)  {
        @NotNull final Project project = findOneByName(userId, name);
        remove(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void add(@NotNull final List<Project> projects) {
        for (@NotNull final Project project: projects) add(project);
    }

    @Override
    public void add(@NotNull final Project... projects) {
        for (@NotNull final Project project: projects) add(project);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public void load(@NotNull List<Project> projects) {
        clear();
        add(projects);
    }

    @Override
    public void load(@NotNull Project... project) {
        clear();
        add(projects);
    }

    @NotNull
    @Override
    public List<Project> getProjectList() {
        return projects;
    }

}