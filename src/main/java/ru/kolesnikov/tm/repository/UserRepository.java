package ru.kolesnikov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.api.repository.IUserRepository;
import ru.kolesnikov.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public @NotNull List<User> findAll() {
        return users;
    }

    @Override
    public @NotNull User add(final @NotNull User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(final @NotNull String id) {
        for (final User user: users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User removeUser(final @NotNull User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User findByLogin(final @NotNull String login) {
        for (final User user: users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeById(final @NotNull String id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User removeByLogin(final @NotNull String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public void add(final @NotNull List<User> users) {
        for (final User user: users) add(user);
    }

    @Override
    public void add(final User... users) {
        for (final User user: users) add(user);
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Override
    public void load(final @NotNull List<User> users) {
        clear();
        add(users);
    }

    @Override
    public void load(final User... users) {
        clear();
        add(users);
    }

}
