package ru.kolesnikov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.api.repository.ITaskRepository;
import ru.kolesnikov.tm.entity.Task;
import ru.kolesnikov.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final @NotNull String userId, final @NotNull Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final @NotNull String userId, final @NotNull Task task) {
        tasks.remove(task);
    }

    @Override
    public @NotNull List<Task> findAll(final @NotNull String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final @NotNull String userId) {
        tasks.clear();
    }

    @Override
    public @NotNull Task findOneById(final @NotNull String userId, final @NotNull String id) {
        for (final Task task: tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public @NotNull Task removeOneById(final @NotNull String userId, final @NotNull String id) {
        Task task = findOneById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public @NotNull Task findOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        return tasks.get(index);
    }

    @Override
    public @NotNull Task findOneByName(final @NotNull String userId, final @NotNull String name) {
        for (final Task task: tasks) {
           if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public @NotNull Task removeOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public @NotNull Task removeOneByName(final @NotNull String userId, final @NotNull String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public @NotNull Task add(final @NotNull Task task) {
        if (task == null) return null;
        tasks.add(task);
        return task;
    }

    @Override
    public void add(final @NotNull List<Task> tasks) {
        for (final Task task: tasks) add(task);
    }

    @Override
    public void add(final Task... tasks) {
        for (final Task task: tasks) add(task);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public void load(final @NotNull List<Task> tasks) {
        clear();
        add(tasks);
    }

    @Override
    public void load(final Task... tasks) {
        clear();
        add(tasks);
    }

    @Override
    public @NotNull List<Task> getTaskList() {
        return tasks;
    }

}