package ru.kolesnikov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task extends AbstractEntity implements Serializable {

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private String userId;

    @Override
    public String toString() {
        return "Task [" +
                "Name='" + name + '\'' +
                ", Description='" + description + '\'' +
                ']';
    }

}