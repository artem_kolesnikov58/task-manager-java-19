package ru.kolesnikov.tm.exception.user;

import ru.kolesnikov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
