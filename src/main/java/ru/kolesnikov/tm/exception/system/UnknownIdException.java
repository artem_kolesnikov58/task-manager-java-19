package ru.kolesnikov.tm.exception.system;

import ru.kolesnikov.tm.exception.AbstractException;

public class UnknownIdException extends AbstractException {

    public UnknownIdException() {
        super("Error! Unknown id...");
    }

}