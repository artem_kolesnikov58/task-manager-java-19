package ru.kolesnikov.tm.exception.system;

import ru.kolesnikov.tm.exception.AbstractException;

public class UnknownNameCommand extends AbstractException {

    public UnknownNameCommand() {
        super("Error! Unknown name...");
    }

}