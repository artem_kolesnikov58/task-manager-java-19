package ru.kolesnikov.tm.command.data.xml.fasterXml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.constant.DataConstant;
import ru.kolesnikov.tm.dto.Domain;
import ru.kolesnikov.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlFasterXmlLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String commandName() {
        return "data-xml-fx-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from XML fasterXML file.";
    }

    @SneakyThrows
    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML FASTERXML LOAD]");
        @NotNull final String xmlData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_XML_FX)));
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final Domain domain = xmlMapper.readValue(xmlData, Domain.class);

        serviceLocator.getDomainService().load(domain);

        System.out.println("[OK]");
        serviceLocator.getAuthService().logout();
        System.out.println("[LOGIN AGAIN]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}