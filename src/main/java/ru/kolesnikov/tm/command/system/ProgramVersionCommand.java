package ru.kolesnikov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.command.AbstractCommand;

public final class ProgramVersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String commandName() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[VERSION]");
        System.out.println("1.0.19");
    }

}