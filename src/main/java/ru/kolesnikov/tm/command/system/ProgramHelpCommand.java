package ru.kolesnikov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.bootstrap.Bootstrap;
import ru.kolesnikov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ProgramHelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String commandName() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[HELP]");
        @NotNull final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        @NotNull final Collection<AbstractCommand> commands = bootstrap.getCommandService().getCommandList();
        for (@NotNull AbstractCommand command : commands) {
            System.out.println(command.commandName() + " - " + command.description());
        }
        System.out.println("[OK]");
    }

}