package ru.kolesnikov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.command.AbstractCommand;

import java.util.List;

public final class ArgumentCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String commandName() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ARGUMENTS]");
        @NotNull final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (@NotNull AbstractCommand command : commands) {
            if (command.arg() == null) continue;
            System.out.println(command.arg());
        }
        System.out.println("[OK]");
    }

}