package ru.kolesnikov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.bootstrap.Bootstrap;
import ru.kolesnikov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String commandName() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[COMMANDS]");
        @NotNull final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        @NotNull final Collection<AbstractCommand> commands = bootstrap.getCommandService().getCommandList();
        for (@NotNull AbstractCommand command : commands) {
            System.out.println(command.commandName());
        }
        System.out.println("[OK]");
    }

}