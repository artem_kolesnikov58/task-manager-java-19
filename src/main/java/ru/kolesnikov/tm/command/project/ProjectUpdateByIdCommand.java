package ru.kolesnikov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.entity.Project;
import ru.kolesnikov.tm.enumerated.Role;
import ru.kolesnikov.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by id.";
    }

    @SneakyThrows
    @Override
    public void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final Project projectUpdated = serviceLocator.getProjectService().updateProjectById(userId, id, name, description);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}