package ru.kolesnikov.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.enumerated.Role;
import ru.kolesnikov.tm.util.TerminalUtil;

public class UserUpdatePasswordCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-update-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Update user password.";
    }

    @SneakyThrows
    @Override
    public void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PASSWORD]");
        System.out.print("ENTER NEW PASSWORD:");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updatePassword(userId, newPassword);
        System.out.println("[THE PASSWORD WAS SUCCESSFULLY CHANGED]");
        serviceLocator.getAuthService().logout();
        System.out.println("[LOG IN TO YOUR ACCOUNT AGAIN]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}