package ru.kolesnikov.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.entity.User;
import ru.kolesnikov.tm.enumerated.Role;

import java.util.List;

public class UserListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String commandName() {
        return "show-user-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show user list.";
    }

    @SneakyThrows
    @Override
    public void execute() throws Exception {
        System.out.println("[USER LIST]");
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        int index = 1;
        for (@NotNull User user : users) {
            System.out.println(index + ". " + user.getLogin());
            index++;
        }

    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}