package ru.kolesnikov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Task task);

    void remove(@Nullable String userId, @Nullable Task task);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    void clear(@Nullable String userId);

    @NotNull
    Task findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task findOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task removeOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task removeOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task updateTaskById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateTaskByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    void load(@Nullable List<Task> tasks);

    void load(@Nullable Task... tasks);

    @NotNull
    List<Task> getTaskList();

}