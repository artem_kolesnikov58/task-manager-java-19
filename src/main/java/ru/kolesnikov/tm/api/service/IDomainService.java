package ru.kolesnikov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.dto.Domain;

public interface IDomainService {

    void load(@Nullable Domain domain);

    void export(@Nullable Domain domain);

}