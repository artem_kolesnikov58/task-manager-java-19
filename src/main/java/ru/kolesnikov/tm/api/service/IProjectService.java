package ru.kolesnikov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Project project);

    void remove(@Nullable String userId, @Nullable Project project);

    @NotNull
    List<Project> findAll(@Nullable String userId);

    void clear(@Nullable String userId);

    @NotNull
    Project findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project findOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project removeOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project removeOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project updateProjectById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Project updateProjectByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    void load(@Nullable List<Project> projects);

    void load(@Nullable Project... projects);

    @NotNull
    List<Project> getProjectList();

}