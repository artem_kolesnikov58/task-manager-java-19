package ru.kolesnikov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.enumerated.Role;

public interface IAuthService {

    @NotNull
    String getUserId();

    void checkRoles(@Nullable Role[] roles);

    boolean isAuth();

    void logout();

    void login(@Nullable String login, @Nullable String password);

    void registry(@Nullable String login, @Nullable String password);

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void updatePassword(@Nullable String userId, @Nullable String newPassword);

    void updateUserFirstName(@Nullable String userId, @Nullable String newFirstName);

    void updateUserLastName(@Nullable String userId, @Nullable String newLastName);

    void updateUserMiddleName(@Nullable String userId, @Nullable String newMiddleName);

    void updateUserEmail(@Nullable String userId, @Nullable String newEmail);

}