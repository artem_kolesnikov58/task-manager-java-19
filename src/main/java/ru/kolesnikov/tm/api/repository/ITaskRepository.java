package ru.kolesnikov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void add(@NotNull String userId, @NotNull Task task);

    void remove(@NotNull String userId, @NotNull Task task);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    @NotNull
    Task findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task removeOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task add(@NotNull Task task);

    void add(@NotNull List<Task> tasks);

    void add(@NotNull Task... tasks);

    void clear();

    void load(@NotNull List<Task> tasks);

    void load(@NotNull Task... tasks);

    @NotNull
    List<Task> getTaskList();

}