package ru.kolesnikov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.kolesnikov.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    @NotNull
    List<AbstractCommand> getCommandList();

}