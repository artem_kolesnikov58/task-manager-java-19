package ru.kolesnikov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    @NotNull
    Project findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project removeOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project add(@NotNull Project project);

    @NotNull
    void add(@Nullable List<Project> projects);

    @NotNull
    void add(@Nullable Project... projects);

    @NotNull
    void clear();

    @NotNull
    void load(@Nullable List<Project> projects);

    @NotNull
    void load(@Nullable Project... projects);

    @Nullable
    List<Project> getProjectList();

}