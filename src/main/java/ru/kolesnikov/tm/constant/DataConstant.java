package ru.kolesnikov.tm.constant;

public class DataConstant {

    public final static String FILE_BINARY = "./data.bin";

    public final static String FILE_BASE64 = "./data.base64";

    public final static String FILE_XML_FX = "./data_fx.xml";

    public final static String FILE_JSON_FX = "./data_fx.json";

    public final static String FILE_XML_JB = "./data_jb.xml";

    public final static String FILE_JSON_JB = "./data_jb.json";

}