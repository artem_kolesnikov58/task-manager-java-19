package ru.kolesnikov.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolesnikov.tm.api.repository.*;
import ru.kolesnikov.tm.api.service.*;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.exception.system.UnknownArgumentException;
import ru.kolesnikov.tm.exception.system.UnknownCommandException;
import ru.kolesnikov.tm.repository.CommandRepository;
import ru.kolesnikov.tm.repository.ProjectRepository;
import ru.kolesnikov.tm.repository.TaskRepository;
import ru.kolesnikov.tm.repository.UserRepository;
import ru.kolesnikov.tm.enumerated.Role;
import ru.kolesnikov.tm.service.*;
import ru.kolesnikov.tm.util.TerminalUtil;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        initCommands(commandService.getCommandList());
    }

    @SneakyThrows
    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void initCommands(@NotNull final List<AbstractCommand> commandList) {
        for (@NotNull AbstractCommand command : commandList) init(command);
    }

    private void init(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commands.put(command.commandName(), command);
        arguments.put(command.arg(), command);
    }

    public void run(@Nullable final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    public static void logError(Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    private void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        authService.checkRoles(command.roles());
        command.execute();
    }

    private void parseArg(@NotNull final String arg) throws Exception {
        if (arg.isEmpty()) return;
        @Nullable final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new UnknownArgumentException(arg);
        argument.execute();
    }

    private boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        if (arg == null) return false;
        try {
            parseArg(arg);
        } catch (Exception e) {
            logError(e);
        }
        return true;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

}